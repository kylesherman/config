conky.config = {
    alignment = 'top_right',
    background = true,
    color2 = '2ECC71',
    cpu_avg_samples = 2,
    default_color = 'E0E2E3',
    double_buffer = true,
    font = 'Bitstream Vera Sans:size=12',
    gap_x = 25,
    gap_y = 45,
    minimum_width = 340,
    no_buffers = true,
    own_window = true,
    own_window_class = 'Conky',
    own_window_type = 'desktop',
    own_window_transparent = true,
    update_interval = 1.0,
    update_interval_on_battery = 5.0,
    use_xft = true,
}

conky.text = [[
# date and time
${voffset 8}${color2}${font Bitstream Vera Sans:size=16}${time %A}${font} \
            ${voffset -8}${alignr}${color}${font Bitstream Vera Sans:size=38}${time %e}${font}
${voffset -30}${color}${font Bitstream Vera Sans:size=18}${time %b}${font} \
              ${voffset -3} ${color}${font Bitstream Vera Sans:size=20}${time %Y}${font}${color2}${hr}

# cpu
#${voffset 10}${color}${font Bitstream Vera Sans:bold:size=12}CPU${font}  ${alignc}${freq_g} GHz  ${alignr}${cpu}%
${voffset 10}${color}${font Bitstream Vera Sans:bold:size=12}CPU${font}  ${alignc}${hwmon 0 temp 1}°C  ${alignr}${cpu}%
${color}${cpubar}
${color}${cpugraph - 15}${color}
#${color Gray}01 [${freq_g 1}] ${cpu 1}% ${cpubar cpu1}${color}
#${color Gray}02 [${freq_g 2}] ${cpu 2}% ${cpubar cpu2}${color}
#${color Gray}03 [${freq_g 3}] ${cpu 3}% ${cpubar cpu3}${color}
#${color Gray}04 [${freq_g 4}] ${cpu 4}% ${cpubar cpu4}${color}
#${color Gray}05 [${freq_g 5}] ${cpu 5}% ${cpubar cpu5}${color}
#${color Gray}06 [${freq_g 6}] ${cpu 6}% ${cpubar cpu6}${color}
#${color Gray}07 [${freq_g 7}] ${cpu 7}% ${cpubar cpu7}${color}
#${color Gray}08 [${freq_g 8}] ${cpu 8}% ${cpubar cpu8}${color}
#${color Gray}09 [${freq_g 9}] ${cpu 9}% ${cpubar cpu9}${color}
#${color Gray}10 [${freq_g 10}] ${cpu 10}% ${cpubar cpu10}${color}
#${color Gray}11 [${freq_g 11}] ${cpu 11}% ${cpubar cpu11}${color}
#${color Gray}12 [${freq_g 12}] ${cpu 12}% ${cpubar cpu12}${color}
${color2}${top name 1}  ${alignr}${top cpu 1}%${color}
${color2}${top name 2}  ${alignr}${top cpu 2}%${color}
${color2}${top name 3}  ${alignr}${top cpu 3}%${color}
${color2}${top name 4}  ${alignr}${top cpu 4}%${color}

# ram
${voffset 10}${color}${font Bitstream Vera Sans:bold:size=12}RAM${font}  ${alignc}${mem} / ${memmax}  ${alignr}${memperc}%
${color}${membar}
${color2}${top_mem name 1}  ${alignr}${top_mem mem_res 1}${color}
${color2}${top_mem name 2}  ${alignr}${top_mem mem_res 2}${color}
${color2}${top_mem name 3}  ${alignr}${top_mem mem_res 3}${color}
${color2}${top_mem name 4}  ${alignr}${top_mem mem_res 4}${color}

# disk
${voffset 10}${color}${font Bitstream Vera Sans:bold:size=12}Disk${font}  ${alignc}${fs_used /} / ${fs_size /}  ${alignr}${fs_used_perc /}%
${color}${fs_bar /}
${color2}I/O:${color}  ${diskio}  ${goto 120}${diskiograph /dev/dm-0 15}
#${color2}RW:${color} ${goto 40}${diskio}  ${goto 120}${diskiograph /dev/dm-0 15}
#${color2}R:${color} ${goto 40}${diskio_read}  ${goto 120}${diskiograph_read /dev/dm-0 15}
#${color2}W:${color} ${goto 40}${diskio_write}  ${goto 120}${diskiograph_write /dev/dm-0 15}

# network
${voffset 10}${color}${font Bitstream Vera Sans:bold:size=12}Network${font}  ${alignr}${color2}Up:${color} ${totalup }  ${color2}Dn:${color} ${totaldown }
${color2}Up:${color}  ${goto 40}${upspeed }  ${goto 120}${upspeedgraph  15}
${color2}Dn:${color}  ${goto 40}${downspeed }  ${goto 120}${downspeedgraph  15}
#${color2}Up${color}  ${alignr}${upspeed }
#${color}${upspeedgraph }
#${color2}Down${color}  ${alignr}${downspeed }
#${color}${downspeedgraph }

# system information
${voffset 10}${color}${font Bitstream Vera Sans:bold:size=12}System${font}
# user
${voffset 5}${color2}User:${color}  ${alignr}${execi 1200 whoami}@${nodename}
# os/distro
${voffset 5}${color2}OS:${color}  ${alignr}${execi 3600 awk -F= '/TION/ {print $2}' /etc/lsb-release | sed 's/"//g'} \
            ${execi 3600 awk -F= '/EASE=/ {printf $2" "} /NAME/ {print $2}' /etc/lsb-release}
# kernel
${voffset 5}${color2}Kernel:${color}  ${alignr}${kernel}
# uptime
${voffset 5}${color2}Uptime:${color}  ${alignr}${uptime_short}
# utc time
${voffset 5}${color2}UTC:${color}  ${alignr}${utime %H:%M}
# packages
${voffset 5}${color2}Packages to Update:${color}  ${alignr}${execi 3600 checkupdates | wc -l}
# entropy
${voffset 5}${color2}Entropy:${color}  ${entropy_avail} / ${entropy_poolsize}  ${entropy_perc}%  ${entropy_bar}

# music
${voffset 10}${color2}Artist:${color}  ${alignr}${mpd_artist}
${color2}Song:${color}  ${alignr}${mpd_title}
]]
